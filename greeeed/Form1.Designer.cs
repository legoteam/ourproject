﻿namespace greeeed
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.listBox6 = new System.Windows.Forms.ListBox();
            this.listBox7 = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.listBox9 = new System.Windows.Forms.ListBox();
            this.listBox8 = new System.Windows.Forms.ListBox();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.listBox10 = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox11 = new System.Windows.Forms.ListBox();
            this.listBox12 = new System.Windows.Forms.ListBox();
            this.listBox13 = new System.Windows.Forms.ListBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.listBox14 = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(550, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 20);
            this.button1.TabIndex = 0;
            this.button1.Text = "Add file";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(4, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(529, 20);
            this.textBox1.TabIndex = 1;
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(6, 81);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(129, 95);
            this.listBox1.TabIndex = 2;
            this.listBox1.Visible = false;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(6, 38);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(615, 40);
            this.button2.TabIndex = 3;
            this.button2.Text = "Scan";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.ForeColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Location = new System.Drawing.Point(178, 81);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.listBox3);
            this.splitContainer1.Size = new System.Drawing.Size(184, 95);
            this.splitContainer1.SplitterDistance = 101;
            this.splitContainer1.TabIndex = 4;
            this.splitContainer1.Visible = false;
            // 
            // listBox2
            // 
            this.listBox2.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox2.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(0, 0);
            this.listBox2.Name = "listBox2";
            this.listBox2.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBox2.Size = new System.Drawing.Size(101, 95);
            this.listBox2.TabIndex = 0;
            // 
            // listBox3
            // 
            this.listBox3.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox3.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(0, 0);
            this.listBox3.Name = "listBox3";
            this.listBox3.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBox3.Size = new System.Drawing.Size(79, 95);
            this.listBox3.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Location = new System.Drawing.Point(11, 213);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.listBox4);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.listBox5);
            this.splitContainer2.Size = new System.Drawing.Size(233, 176);
            this.splitContainer2.SplitterDistance = 115;
            this.splitContainer2.TabIndex = 5;
            this.splitContainer2.Visible = false;
            // 
            // listBox4
            // 
            this.listBox4.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox4.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox4.FormattingEnabled = true;
            this.listBox4.Location = new System.Drawing.Point(0, 0);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(115, 176);
            this.listBox4.TabIndex = 0;
            // 
            // listBox5
            // 
            this.listBox5.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox5.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox5.FormattingEnabled = true;
            this.listBox5.Location = new System.Drawing.Point(0, 0);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(114, 176);
            this.listBox5.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Location = new System.Drawing.Point(12, 404);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.listBox6);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.listBox7);
            this.splitContainer3.Size = new System.Drawing.Size(597, 213);
            this.splitContainer3.SplitterDistance = 466;
            this.splitContainer3.TabIndex = 6;
            this.splitContainer3.Visible = false;
            // 
            // listBox6
            // 
            this.listBox6.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox6.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox6.FormattingEnabled = true;
            this.listBox6.HorizontalScrollbar = true;
            this.listBox6.Location = new System.Drawing.Point(0, 0);
            this.listBox6.Name = "listBox6";
            this.listBox6.Size = new System.Drawing.Size(466, 213);
            this.listBox6.TabIndex = 0;
            this.listBox6.SelectedIndexChanged += new System.EventHandler(this.listBox6_SelectedIndexChanged);
            // 
            // listBox7
            // 
            this.listBox7.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox7.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox7.FormattingEnabled = true;
            this.listBox7.HorizontalScrollbar = true;
            this.listBox7.Location = new System.Drawing.Point(0, 0);
            this.listBox7.Name = "listBox7";
            this.listBox7.Size = new System.Drawing.Size(127, 213);
            this.listBox7.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(720, 62);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 100);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(536, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "Version 0.1";
            this.label1.UseMnemonic = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(911, 213);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(82, 175);
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // listBox9
            // 
            this.listBox9.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox9.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox9.FormattingEnabled = true;
            this.listBox9.Location = new System.Drawing.Point(0, 0);
            this.listBox9.Name = "listBox9";
            this.listBox9.Size = new System.Drawing.Size(432, 174);
            this.listBox9.TabIndex = 0;
            // 
            // listBox8
            // 
            this.listBox8.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox8.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox8.FormattingEnabled = true;
            this.listBox8.Location = new System.Drawing.Point(0, 0);
            this.listBox8.Name = "listBox8";
            this.listBox8.Size = new System.Drawing.Size(75, 174);
            this.listBox8.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Location = new System.Drawing.Point(343, 215);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.listBox8);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.listBox9);
            this.splitContainer4.Size = new System.Drawing.Size(511, 174);
            this.splitContainer4.SplitterDistance = 75;
            this.splitContainer4.TabIndex = 10;
            this.splitContainer4.Visible = false;
            // 
            // listBox10
            // 
            this.listBox10.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox10.ForeColor = System.Drawing.SystemColors.Window;
            this.listBox10.FormattingEnabled = true;
            this.listBox10.Location = new System.Drawing.Point(250, 215);
            this.listBox10.Name = "listBox10";
            this.listBox10.Size = new System.Drawing.Size(87, 173);
            this.listBox10.TabIndex = 11;
            this.listBox10.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(626, 404);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 12;
            // 
            // listBox11
            // 
            this.listBox11.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox11.ForeColor = System.Drawing.SystemColors.Info;
            this.listBox11.FormattingEnabled = true;
            this.listBox11.Items.AddRange(new object[] {
            "SI.FileCreationTime",
            "System Time",
            "SI.FileAlterationTime",
            "System Time",
            "SI.MFTChangeTime",
            "System Time",
            "SI.FileReadTime",
            "System Time",
            "FI.FileCreationTime",
            "System Time",
            "FI.FileModificationTime",
            "System Time",
            "FI.FileAccessTime",
            "System Time",
            "FI.MFTModificationTime",
            "System Time"});
            this.listBox11.Location = new System.Drawing.Point(615, 405);
            this.listBox11.Name = "listBox11";
            this.listBox11.Size = new System.Drawing.Size(126, 208);
            this.listBox11.TabIndex = 13;
            this.listBox11.Visible = false;
            // 
            // listBox12
            // 
            this.listBox12.BackColor = System.Drawing.SystemColors.MenuText;
            this.listBox12.ForeColor = System.Drawing.SystemColors.Info;
            this.listBox12.FormattingEnabled = true;
            this.listBox12.Location = new System.Drawing.Point(747, 405);
            this.listBox12.Name = "listBox12";
            this.listBox12.Size = new System.Drawing.Size(107, 212);
            this.listBox12.TabIndex = 14;
            this.listBox12.Visible = false;
            // 
            // listBox13
            // 
            this.listBox13.FormattingEnabled = true;
            this.listBox13.Location = new System.Drawing.Point(11, 623);
            this.listBox13.Name = "listBox13";
            this.listBox13.Size = new System.Drawing.Size(485, 134);
            this.listBox13.TabIndex = 15;
            this.listBox13.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(860, 596);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(74, 20);
            this.textBox2.TabIndex = 16;
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.Location = new System.Drawing.Point(940, 594);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(74, 24);
            this.button3.TabIndex = 17;
            this.button3.Text = "HEX it";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // listBox14
            // 
            this.listBox14.FormattingEnabled = true;
            this.listBox14.Location = new System.Drawing.Point(502, 623);
            this.listBox14.Name = "listBox14";
            this.listBox14.Size = new System.Drawing.Size(509, 134);
            this.listBox14.TabIndex = 18;
            this.listBox14.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(860, 580);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "HEX VIEW";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1023, 782);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listBox14);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.listBox13);
            this.Controls.Add(this.listBox12);
            this.Controls.Add(this.listBox11);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox10);
            this.Controls.Add(this.splitContainer4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.splitContainer3);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.Name = "Form1";
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.ListBox listBox5;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.ListBox listBox6;
        private System.Windows.Forms.ListBox listBox7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ListBox listBox9;
        private System.Windows.Forms.ListBox listBox8;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.ListBox listBox10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBox11;
        private System.Windows.Forms.ListBox listBox12;
        private System.Windows.Forms.ListBox listBox13;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ListBox listBox14;
        private System.Windows.Forms.Label label3;

    }
}

